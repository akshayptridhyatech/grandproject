$(document).ready(function() {
	$("#loginform").validate({
		rules:{
	        email: {
	        	required :true,
	        	email : true
	        },
	        password: {
	        	required : true
	        }
        },
	    messages: {
	        email :{
                required : "Email is required"
            },
            password : {
            	required : "Password is required"
            } 
	    }
    });
    $('#forgotpassword').validate({
    	rules: {
    		email:{
    			required: true
    		}
    	},
    	messages:{
    		email:{
    			required:"Email is required"
    		}
    	}
    });
    $('#register').validate({
    	rules:{
    		name:{
    			required: true
    		},
    		email:{
    			required:true
    		},
    		gender:{
    			required:true
    		},
    		address:{
    			required:true
    		},
    		contact:{
    			required:true
    		},
    		image:{
    			required:true
    		},
    		password:{
    			required:true
    		},
    		confirm_password:{
    			equalTo:"#password"
    		}
    	},
    	messages:{
    		name:{
    			required : "Name is required"
    		},
    		email :{
                required : "Email is required"
            },
            address:{
            	required : "Address is required"
            },
            image :{
            	required : "Image is required"
            },
            contact : {
            	required : "Contact is required"
            },
            password : {
            	required : "Password is required"
            },
            confirm_password : {
        		equalTo : "Both password should same"
        	} 
    	}
    });
    $('#resetpasswordform').validate({
    	rules:{
    		password:{
    			required:true
    		},
    		confirm_password:{
    			equalTo:"#password"
    		}
    	},
    	messages :{
    		password : {
            	required : "Password is required"
            },
            confirm_password : {
        		equalTo : "Both password should same"
        	} 
    	}
    });
});
