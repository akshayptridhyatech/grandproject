$('.delete-category').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Are you sure?',
            text: 'This category and it`s details will be permanantly deleted!',
            icon: 'warning',
            buttons: ["No", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
    });

$('.delete-order').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('href');
        swal({
            title: 'Are you sure?',
            text: 'This order and it`s details will be permanantly deleted !',
            icon: 'warning',
            buttons: ["No", "Yes!"],
        }).then(function(value) {
            if (value) {
                window.location.href = url;
            }
        });
});

$(document).ready(function(){
    $('.customSwitchecategory').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let cat_Id = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/status/update',
            data: {'status': status, 'cat_Id': cat_Id},
            success: function (data) {
              if(data.success===true)
              {
                swal(data.msg); 
                $('#customSwitches_'+cat_Id).prop('checked', true);
              }
              if(data.success=== false)
              {
                swal(data.msg);
              }
            }
        });
    });


     $('.customSwitcheorder').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let orderId = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/status/updateorder',
            data: {'status': status, 'orderId': orderId},
            success: function (data) {
              if(data.success === true)
              {
                swal(data.msg);
              }
            }
        });
    });

});
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
function duplicateCategory(id){
            var category_name =$('#category_name').val();
           
            $.ajax({
            type: "POST",
            url: '/checkCategory',
            data: {
            id: id,
             category_name:category_name
            },
            dataType: "json",
            success:function(res) {
                if(res.exists){
                  $('.text-danger').html('This Category Already Exist!!');
                }else{
                  $('.text-danger').html('');
                  $( "#categoryform" ).submit();
                }
            },
            error: function (jqXHR, exception) {
            }
        }); 
}
function checkduplicateCategory(){
            var category_name = $('#category_name').val();
            $.ajax({
            type: "POST",
            url: '/checkCategory',
            data: {
            category_name:category_name
            },
            dataType: "json",
            success:function(res) {
                if(res.exists){
                  $('.text-danger').html('This Category Already Exist!!');

                }else{
                  $('.text-danger').html('');
                  $( "#categoryform" ).submit();
                }
            },
            error: function (jqXHR, exception) {
            }
        }); 
}