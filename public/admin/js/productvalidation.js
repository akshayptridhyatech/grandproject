 $(document).ready(function () {
 $('#productform').validate({

                rules: {
                   
                   category : {
                      required: true,
                        
                    },
                   

                    p_name: {
                     required: true,
                                        
                   },
                  description:{
                       required: true,
                  },
                   p_quantity:{
                       required: true,
                  },
                   p_price:{
                      required: true,
                  }
          },
          messages:{
                   
                category: {
                  required: " category required",
                   


                    },
                     p_name: {
                 
              required: " product name required",

                  },
                   description: {
                 
                   required: " description required",

                  },
                   p_quantity: {
                 
                  required: " product quantity required",

                  },
                  p_price: {
                 
               required: " product price required",

                  }
              }
                       
            });
  });

 $(document).ready(function(){
    $('.customSwitchesproduct').change(function () {
        var status = $(this).prop('checked') === true ? 1 : 0;


        var productId = $(this).attr('data-id');
          // alert(productId);
       
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '/productstatus',
            data: {'status': status, 'product_id':productId },
            success: function (data) {
             if (data.success === true) {
                            swal(data.message);
                        } 
              
               
            }
        });
    });

    $('.delete-product').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
   

    swal({
        title: 'Are you sure?',
        text: 'This record and it`s details will be permanantly deleted!',
        icon: 'warning',
        buttons: ["Cancel", "Yes!"],
    }).then(function(value) {
        if (value) {
           
            window.location.href = url;
        }
    });
});

     $('.deleteimage').on('click', function (event) {
    event.preventDefault();
    var id = $(this).attr('data-attr');
   
   

    $.ajax({
            type: "GET",
            dataType: "json",
            url: '/delteimageinupdate',
            data: { 'image_id':id },
            success: function (data) {
              // alert(data);
             if (data.success === true) {
                    swal(data.message);
                 } 
                 $('.hide_'+id).html('');
                
                 
              
               
            }
        });
});  
});
 