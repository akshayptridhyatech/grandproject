
$(document).ready(function() {
	$("#forgotpassword").validate({
		rules:{
	        email: {
	        	required :true,
	        	email : true
	        },
        },
	    messages: {
	        email :{
                required : "Email is required"
            } 
	    }
    });
    $("#changepasswordForm").validate({
        rules: {
        	password : {
        		required : true,
        	},
        	re_enter_password : {
        		equalTo: "#password"
        	}
        },
        messages:{
        	password : {
        		required : "Password is required"
        	},
        	re_enter_password : {
        		equalTo : "Both password should same"
        	}
        }
    });
    $("#updateprofile").validate({
        rules: {
            name:{
                required : true,
            },
            contact :{
                required : true,
            },
            email : {
                email : true ,
                required : true
            },
            address : {
                required : true
            }

        },
        messages:{
            name:{
                required : "Name is required"
            },
            contact : {
                required : "Contact is required"
            },
            email : {
                required : "Email is required"
            },
            address : {
                required : "Address is required"
            }
        }
    });
    $("#loginForm").validate({
    	rules: {
    		email:{
    			required : true,
    			email:true
    		},
    		password:{
    			required : true
    		}
    	},
    	messages:{
    		email : {
    			required : "Email is required"
    		},
    		password:{
    			required : "password is required"
    		}
    	}
    })
    $('.delete_user').on('click', function (event) {
        event.preventDefault();
        const url = $(this).attr('data-attr');
        swal({
            title: 'Are you sure?',
            text: 'This record and it`s details will be permanantly deleted!',
            icon: 'warning',
            buttons: ["Cancel", "Yes!"],
        }).then(function(value) {
            if (value) {
                $.ajax({
	        	url:'/delete/'+url,
	        	method:'get',
	        	success: function(result) {
	        		location.reload();
	            },
	        })
            }
        });
	});

});
