<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\product;
use App\Models\productimage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Mail\forgotpasswordmail;
use Carbon\Carbon;
use File;
use DB;
use Config;
use Response;
use JsValidator;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    //registration form
    public function create()
    {
        return view('Front.user.register');
    }

    //login form
    public function viewlogin()
    {
        return view('Front\user\loginuser');
    }

    //authenticate user
    public function authenticateuser(Request $request)
    {
         $this->validate($request,[
            'email' => 'required',
            'password' => 'required' 
        ]);
        
        //echo "<pre>";print_r($request->all());exit;
        $userdata = array(
            'password'  => $request->password,
            'email'     => $request->email
            );
         //print_r($userdata);exit;
            if(Auth::attempt($userdata,true)) {
                return redirect()->guest('index')->with(Auth::User()->id,Auth::User()->name);
            }else{
                return back()->with('fail','Incorrect email or password');
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validationRules=[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'contact' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password'=>'required',
            'address' => 'required',
            'confirm_password'=>'required|same:password',
        ];

        if($request->gender=="")
        {
            $validationRules['gender'] = 'required';
        }

        $validation = Validator::make($request->all(),$validationRules);

        if ($validation->fails()) {
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $path = $request->file('image');
        $filename = time() . '.' . $path->getClientOriginalExtension();
        $directoryPath=public_path('/upload_profile');

        if(File::isDirectory($directoryPath)){
            $path->move(public_path('/upload_profile'), $filename);
        }else{
            File::makeDirectory($directoryPath);
            $path->move(public_path('/upload_profile'), $filename);
        }

        $user=new User();
        $user->name=$request->name;
        $user->email =$request->email;
        $user->image=$filename;
        $user->gender=$request->gender;
        $user->address=$request->address;
        $user->contact = $request->contact;
        $user->password=Hash::make($request->password);
        $user->role_id=2;
        $save = $user->save();

        if($save){

                alert()->success('You are registered successfully')->persistent('close')->autoclose("3600");

                 return redirect()->guest('signin');
            }else{
                return back()->with('error','error');
            }
    }

    //return index page
    public function viewindex()
    {
       
        $product = product::with('first_pic')->get();
         //echo '<pre>';print_r($product);exit;
        // $productimage =productimage::where('product_id',$product->id)->get();
        // echo '<pre>';print_r($productimage);exit;
        return view('Front.user.index',compact('product'));
    }

    //form of forgotpassword
    public function getmail()
    {
        return view('Front.user.getemail');
    }

    //send link to user for reset password
    public function sendmailtouser(Request $request)
    {
        $validationRules=[
            'email' => 'required'
        ];
        $validation = Validator::make($request->all(),$validationRules);
       
        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
            $user=User::select('id')->where('email',$request['email'])->first();
            if($user!=null){
                \Mail::to($request['email'])->send(new \App\Mail\forgotpasswordmail($user));
                alert()->info('Please check your mail')->persistent('close')->autoclose("3600");
                return redirect('/signin');
            }else{
                return redirect()->back()->with('error','User not exist')->withInput();
            }
        }
    }

    //view form of reset password
    public function resetpassword($id)
    {
        return view('Front.user.resetpassword',compact('id'));
    }

     //forgot password change
    public function newpasswordofuser(Request $request){
        $validationRules=[
            'password' => 'required',
            'confirm_password' => 'required|same:password'
         ];

        $validation = Validator::make($request->all(),$validationRules);

        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
            $id=$request->id;
            $user=User::find($id);
            $user->password=Hash::make($request->password);
            $save=$user->save();
            if($save){
                alert()->success('Your password is changed successfully ')->persistent('close')->autoclose("3600");
                return redirect()->guest('signin');
                
            }else{
                return back()->with('error','error');
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
