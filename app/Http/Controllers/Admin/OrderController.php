<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use File;
use DB;
use Config;
use Response;
use JsValidator;

class OrderController extends Controller
{
    
    public function show(Request $request)
    {

        $ordersearch = trim($request->ordersearch);
        $dis=Order::select('orders.*','product.product_name','users.name')->join('product','product.id','orders.product_id')->join('users','users.id','orders.user_id');
        if(isset($request->ordersearch) && !empty($request->ordersearch))
        {
             $display=$dis->sortable()->where('product.product_name','like','%'.$request->ordersearch.'%')->paginate(3);      
        }else{
            $display=$dis->sortable()->paginate(3);
        }
    	return view('admin.order.vieworder',compact('display','ordersearch'));
    }

    public function destroy($id)
    {
        $order_id = Order::find($id);
        $order_id->delete();
        return redirect()->guest('vieworder')->with('success','Order Cancel');
    }

    public function updateStatus(Request $request)
    {

        $userorder = Order::findOrFail($request->orderId);
        $userorder->order_status = $request->status;
        $save=$userorder->save();
        if($save)
        {
          $success=true;
          $msg="Order status updated successfully";
        }
       return response()->json(['success'=>$success,'msg' => $msg]);
    }

}
