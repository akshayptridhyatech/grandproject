<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use File;
use DB;
use Config;
use Response;
use JsValidator;

class CategoryController extends Controller
{
    /*this function add the category*/
    public function addcategory(Request $request)
    {

        $validationRules =  [
           'category_name' => 'required|unique:categories,category_name,NULL,id,deleted_at,NULL'
        ];
        $validation=Validator::make($request->all(), $validationRules);
        if($validation->fails()) {    
            return redirect()->back()->withErrors($validation->errors())->withInput();
        }

        $category=new Category();
        $category->category_name=$request->category_name;
        $category->category_status=1;
        $categoryadd=$category->save();

        if($categoryadd)
        {
        alert()->success('Product Category Is Added')->persistent('close')->autoclose("36000");
        return redirect()->guest('viewproductcategory')->with('success','data update');
        }
        else
        {
         alert()->warnning('Product Category Is Not Added')->persistent('close')->autoclose("36000"); 
         return back()->with('warnning','Data not added');
        }

     /* return redirect()->guest('viewproductcategory')->with('success','data update');*/
    }

    /*this function show list of the category*/
    public function show(Request $request)
    {

        $sname = trim($request->catsearch);
        $display=Category::get();

        if(isset($request->catsearch) && !empty($request->catsearch))
        {
             $display=Category::where('category_name','like','%'.$request->catsearch.'%')->latest()->paginate(4);

        }else
        {
            $display=Category::latest()->paginate(4);
        }
    
    	return view('admin.category.viewproductcategory',compact('display','sname'));


    }

    /*this function display edit category form*/
    public function edit($id)
    {
         $editcategory = Category::find($id);
         return view('admin.category.updatecategory',compact('editcategory'));
    }
    
    /*this function update the category name*/
    public function update(Request $request, $id)
    {

          $updatecategory = Category::find($id);
    	  $this->validate($request,[
            'category_name' => 'required|unique:categories,category_name,'.$updatecategory->id
        ]);
       
        $updatecategory->category_name = $request->category_name;
        $updatecategory->save();

        alert()->success('Product Category Is Updated')->persistent('close')->autoclose("36000");

        return redirect()->guest('viewproductcategory')->with('success','data update');
    }

    /*this function delete the category*/
    public function destroy($id)
    {

        $category_id = Category::find($id);
        $check=Product::where('category_id',$category_id->id)->get();
        if(!$check->isEmpty())
        {  
             alert()->info('First Remove Products Of This Category')->persistent('close')->autoclose("36000");
        }else
        {
           $category_id->delete();
        }
        return redirect()->guest('viewproductcategory')->with('success','Post deleted successfully');
    }

    /*this function update status of category according to stock of category*/
    public function updateStatus(Request $request)
    {
      //  echo "<pre>";print_r($request->all());exit;

        $category = Category::find($request->cat_Id);
        $category->category_status = $request->status;
        $check=Product::where('category_id',$category->id)->get();
    // /   echo "<pre>";print_r($check);exit;
        if(!$check->isEmpty())
        {  
         $success=true;
         $msg="First Remove Products Of This Category";
        }else
        {
          $save=$category->save();
          $success=false;
          $msg="Category Status Updated";
         
        }
        return response()->json(['success'=>$success,'msg'=> $msg]);
    }

    public function checkCategory(Request $request){

        $category_name = $request->input('category_name');
        $Exists= Category::where('category_name',$category_name)->where('deleted_at','==',NULL);
        if($request->input('id'))
        {
            $Exists->where('id','!=',$request->input('id'));
        }
        $isExists=$Exists->first();
        if($isExists){
            return response()->json(array("exists" => true));
        }else{
            return response()->json(array("exists" => false));
        }
    }


}
