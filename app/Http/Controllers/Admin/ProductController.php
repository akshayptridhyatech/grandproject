<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\productimage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
 use Config;
use File;
use DB;
use JsValidator;
use Validator;

class ProductController extends Controller
{
  
      /*return view of add product */

   		public function addproduct()
        {
     	  $category = Category::where('category_status',1)->get();
   			 return view('admin.product.addproduct',compact('category'));
  		 }


       
      /*function for insert product in database */

       public function insertproduct(Request $request)
         {
    
           $validationRules =[
		            'category' => 'required',
		            'p_name' => 'required',
		            'description' => 'required',
		            'p_quantity' => 'required',
		            'p_price' => 'required',
		            'file' => 'required',
		             'file.*' => 'mimes:jpg,jpeg,png'


          			 ];

      
		          if(isset($request['category']) && $request->category=='')
		              {
		              	$validationRules['category']='required';
		              }
		    
		           $validation = Validator::make($request->all(), $validationRules);
       	
         

		           if ($validation->fails()) {
		            return redirect()->back()->withInput()->withErrors($validation->errors());
		            }
		          

    
		   
		      $product=new Product();
		      $product->category_id=$request->category;
		      $product->product_name=$request->p_name;
		      $product->product_description=$request->description;
		      $product->product_quantity=$request->p_quantity;
		      $product->product_price=$request->p_price;
		     // $product->product_image=implode(",",$images);

		      $product->product_status=1;
		   	  $save=$product->save();
		  
                 if($files=$request->file('file')){

                foreach($files as $file){
		          
		            $filename =  $file->getClientOriginalName();
					$path = public_path().'/upload';
					if (! File::exists($path)) {
					File::makeDirectory($path);
					$file->move(public_path('/upload'), $filename);
					}
					else
					{
					$file->move(public_path('/upload'), $filename);
	                }

		          
	                  $image = new productimage();
   			         $image->product_id=$product->id;
			   	      $image->image_name=$filename;
			   	       $image->save();
 
			       

      			  }

                  
      			 
   			 }



		      if($save)
              {
                    alert()->success(' Product Added successfully ')->persistent('close')->autoclose("36000");
                     return redirect()->guest('getproduct');
                  
               }
            else
                {
                	  alert()->warnning('Your Product is not Added')->persistent('close')->autoclose("36000");
                     return back()->withInput()->with('warnning','Data not added');
                }


              
		  
        
        


      }


              
         /*function getproduct is for list product details */
  
         public function getproduct(Request $request) 
         {
        	 
        	 
            $getquery = Product::select('product.*','categories.category_name')->with('setimages')->join('Categories','Categories.id','product.category_id')->where('categories.category_status',1);
        	  
            $filter=$request['sproduct'];

            if(isset($request['sproduct']) && !empty($request['sproduct']))
              {
                  
                $product1 = $getquery->where('product_name', 'like', '%'.$request['sproduct'].'%')->latest()->paginate(3);
              }
              else
              {
              	 $product1 = $getquery->latest()->paginate(3);
              }
        
     
             return view('Admin.product.viewproduct',compact('product1','filter'));
   		 }
            
            
            

             
       
      
        /*viewedit function for get id of product for update opration*/

         public function viewedit($id)
         {

       	 $category = Category::where('category_status',1)->get();
       	 $images = productimage::where('product_id',$id)->get();
          $data = Product::find($id);

            return view('Admin.product.editproduct',compact('data','category','images'));

         }


          /*updateproduct function for update product details in database*/

         public function updateproduct(Request $req)
         {
           $validate =[
            'category' => 'required',
            'p_name' => 'required',
            'description' => 'required',
            'p_quantity' => 'required',
            'p_price' => 'required',
          ];

         if(isset($request['category']) && $request->category=='')
              {
              	$validationRules['category']='required';
              }

        
           $validation = Validator::make($req->all(), $validate);
           if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors()->withInput());
            }
              

         	
               

  
   			

             $updatedata = Product::find($req->product_id);
              $updatedata->category_id=$req->category;
		      $updatedata->product_name=$req->p_name;
		      $updatedata->product_description=$req->description;
		      $updatedata->product_quantity=$req->p_quantity;
		      $updatedata->product_price=$req->p_price;
		      $updatedata->product_status=1;
		     
		      $update =$updatedata->save();
        

                        if($files=$req->file('file')){

                foreach($files as $file){
		          
		            $filename =  $file->getClientOriginalName();
					$path = public_path().'/upload';
					if (! File::exists($path)) {
					File::makeDirectory($path);
					$file->move(public_path('/upload'), $filename);
					}
					else
					{
					$file->move(public_path('/upload'), $filename);
	                }
	                

		               //$imageupdate =productimage::where('unit_id', '=', $unit_store_id)->get();
	                  $imageupdate =  new productimage();
	                // echo'<pre>'; print_r($imageupdate);exit;
   			          $imageupdate->product_id=$updatedata->id;
			   	      $imageupdate->image_name=$filename;
			   	       $imageupdate->save();
 
			       

      			  }

                  
      			 
   			 }

		       if($update)
                   {
                    alert()->success('Your Data Updated Successfully ')->persistent('close')->autoclose("36000");
                     return redirect('getproduct');
                  
                   }
               else
                   {
                	  alert()->warning('Your Data Is Not Updated')->persistent('close')->autoclose("36000");
                     return back()->with('warnning','Data not added');
                   }

         }



         /*function for delete product*/
           public  function deleteproduct($id)
            {
           
		       $deleltedata = Product::find($id);
		       $deleltedata->delete();
		       $delete = productimage::where('product_id',$id)->delete();
               return redirect('getproduct');
           } 
		       
            
           /*function for change product status*/
           public function productstatus(Request $request)
             {
          	   $product = Product::find($request->product_id);
          	
               $product->product_status = $request->status;
               $status=$product->save();

               if ($status) {
	            $success = true;
	            $message = " Product Status Updated Successfully";
        
                }
                  return response()->json(['success' => $success,
                  'message' => $message,]);
             } 

             public function delteimageinupdate(Request $request)
             {
             	$image = productimage::find($request->image_id);
             	$deleteimage=$image->delete();

             	if ($deleteimage) {
	            $success = true;
	            $message = " Image Deleted Successfully";
        
                }
                  return response()->json(['success' => $success,
                  'message' => $message,]);
             
             } 


             

}
       
           

        
           
           



		        
		     
          








       
                
             
            
           
       
        
           
            
         
          
           
            
         

       
