<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use File;
use DB;
use Config;
use Response;
use JsValidator;

class userController extends Controller
{
	public function __construct()
	{

	}
	//view user
    public function index(Request $request)
    {
        // print_r($request->email);exit;
        $dataQuery =  User::select()->where('role_id','!=','1');
        if(isset($request['search']) && !empty($request['search'])){
        // echo "<pre>";print_r($request->all());exit;
            $dataQuery->where('name','like',$request['search'].'%');
        }
        $users=$dataQuery->latest()->paginate(3);
        $user_count=User::where('role_id','!=','1')->count();
        $order_count=Order::count();
        $new_order=Order::whereDate('created_at',date("Y-m-d"))->count();
    	return view('admin.user.dashboard',compact('users','request','user_count','new_order','order_count'));
    }

    //delete user
    public function destroy($id)
    {
    	$user = User::find($id);
        $user->delete();

        return redirect()->guest('dashboard');

    }

    //form of reset password
    public function resetpassword($id)
    {
        return view('admin.user.resetpassword',compact('id'));
    }

    //send mail for forgot password
    public function sendmail(Request $request)
    {
        $validationRules=[
            'email' => 'required'
        ];
        $validation = Validator::make($request->all(),$validationRules);
       
        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
            $user=User::select('id')->where('email',$request['email'])->first();
            if($user!=null){
                \Mail::to($request['email'])->send(new \App\Mail\MyTestMail($user));
                alert()->info('Please check your mail')->persistent('close')->autoclose("3600");
                return redirect('/login');
            }else{
                return redirect()->back()->with('error','User not exist')->withInput();
            }
        }
    }

    //forgot password change
    public function newpassword(Request $request){
        $validationRules=[
            'password' => 'required',
            're_enter_password' => 'required|same:password'
         ];

        $validation = Validator::make($request->all(),$validationRules);

        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
            $id=$request->id;
            $user=User::find($id);
            $user->password=Hash::make($request->password);
            $save=$user->save();
            if($save){
                alert()->success('Your password is changed successfully ')->persistent('close')->autoclose("3600");
                return redirect()->guest('login');
                
            }else{
                return back()->with('error','error');
            }
        }
    }
    
    //change password
    public function updatepassword(Request $request,$id="")
    {
    	$validationRules=[
        	'password' => 'required',
        	're_enter_password' => 'required|same:password'
         ];

        $validation = Validator::make($request->all(),$validationRules);

        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
    		$user=User::find($id);
    		$user->password=Hash::make($request->password);
    		$save=$user->save();
            if($save){
                alert()->success('Your password is changed successfully ')->persistent('close')->autoclose("3600");
                return redirect()->guest('dashboard');
                
            }else{
                return back()->with('error','error');
            }
    	}
    }

    public function editprofile($id)
    {
        $user=User::find($id);
        return view('admin.user.editprofile',compact('user'));
    }

    public function updateprofile(Request $request,$id)
    {
        $user=User::find($id);
        $validationRules=[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'address' => 'required',
            'contact' => 'required|max:10',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
        if($request->gender=="")
        {
            $validationRules['gender'] = 'required';
        }
        $validation = Validator::make($request->all(),$validationRules);
       
        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
        
            if($request->hasFile('image')){
                $path = $request->file('image');
                //$iname=$path->getClientOriginalName();
                $filename = time() . '.' . $path->getClientOriginalExtension();
                $directoryPath=public_path('/upload');
                if(File::isDirectory($directoryPath))
                {
                    $path->move(public_path('/upload'), $filename);
                }
                else{
                    File::makeDirectory($directoryPath,0777,true,true);
                    $path->move(public_path('/upload'), $filename);
                }
                $user->image=$filename;
            }
            $user->name=$request->name;
            $user->email=$request->email;
            $user->address=$request->address;
            $user->contact=$request->contact;
            $user->gender=$request->gender;
            $save=$user->save();
            if($save){
                alert()->success('Your data updated successfully')->persistent('close')->autoclose("3600");
                return redirect()->guest('dashboard');
            }else{
                return back()->with('error','error');
            }
        }
    }
}
