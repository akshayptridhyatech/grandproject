<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use File;
use DB;
use Config;
use Response;
use JsValidator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

    public function sendmailtolapshop(Request $request)
    {
        $validationRules=[
            'email' => 'required',
            'name'=>'required',
            'message'=>'required'
        ];
        $validation = Validator::make($request->all(),$validationRules);
       
        if ($validation->fails()){
             return redirect()->back()->withErrors($validation->errors())->withInput();
        }else{
            $user=User::select('email')->where('role_id','1')->first();
           /*  print_r($user->email);exit();*/
            if($user!=null){
                \Mail::to($user->email)->send(new \App\Mail\contactMail($request));
                alert()->info('Please check your mail')->persistent('close')->autoclose("3600");
                return redirect()->back();
            }else{
                return redirect()->back()->with('error','User not exist')->withInput();
            }
        }
    }



   public function addproduct()
   {
    return view('admin/addproduct');
   }


    public function checkuser(Request $request)
    {
        $this->validate($request,[
            'email' => 'required',
            'password' => 'required' 
        ]);
        
        //echo "<pre>";print_r($request->all());exit;
        $userdata = array(
            'password'  => $request->get('password'),
            'email'     => $request->get('email')
            );
         //print_r($userdata);exit;
            if(Auth::attempt($userdata,true)) {
                return redirect()->guest('dashboard')->with(Auth::User()->id,Auth::User()->name);
            }else
            {
                return back()->with('fail','Incorrect password');
            }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->guest('login');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
