<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Order extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable =['order_status','quantity','amount'];
    use SoftDeletes;

    protected $table="orders";

    public $sortable = ['amount'];

}
