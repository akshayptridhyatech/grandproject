<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
    use HasFactory;
use SoftDeletes;
    protected $table="product";

     protected $fillable = [ 'category_id', 'product_name','product_description','product_quantity', 'product_price','product_status'];

       public function setimages()
    {
        return $this->hasMany(productimage::class);
    }

    public function first_pic() {
         return $this->hasOne(productimage::class)->oldest();
}


}

