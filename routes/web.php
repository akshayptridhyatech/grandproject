<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('admin/user/login');
});
Route::get('/forgotpassword', function () {
    return view('admin/user/forgotpassword');
});

Route::post('checkuser', 'AdminController@checkuser');
Route::post('sendmail','Admin\userController@sendmail');
Route::get('/resetpassword/{id}','Admin\userController@resetpassword');
Route::post('newpassword','Admin\userController@newpassword');

Route::get('/register', 'Front\userController@create');
Route::post('/adduser', 'Front\userController@store');
Route::get('/signin', 'Front\userController@viewlogin');
Route::post('/authenticateuser','Front\userController@authenticateuser');
Route::get('/index', 'Front\userController@viewindex');
Route::get('getmail','Front\userController@getmail');
Route::post('/sendmailtouser', 'Front\userController@sendmailtouser');
Route::post('/sendmailtolapshop', 'AdminController@sendmailtolapshop');
Route::get('/resetpasswordofuser/{id}','Front\userController@resetpassword');
Route::post('/newpasswordofuser','Front\userController@newpasswordofuser');

Route::group(['middleware' => ['auth']], function() {

	Route::get('/viewproduct', function () {
	    return view('admin/viewproduct');
	});

	Route::any('viewfeedback',function(){
		return view('admin/viewfeedback');
	});

	Route::get('addcategory',function(){
		return view('admin/category/addcategory');
	});

	Route::any('viewproductcategory',function(){
		return view('admin/viewproductcategory');
	})->name('viewcategory');

	Route::get('contactus', function () {
	    return view('Front/contactus');
	});

	Route::get('aboutus', function () {
	    return view('Front/aboutus');
	});

	Route::post('insertcategory','Admin\CategoryController@addcategory');
	Route::get('viewproductcategory','Admin\CategoryController@show');
	Route::get('deletecategory/{id}','Admin\CategoryController@destroy');
	Route::get('editcategory/{id}', 'Admin\CategoryController@edit');
	Route::post('/update/{id}', 'Admin\CategoryController@update');
	Route::get('/status/update', 'Admin\CategoryController@updateStatus')->name('categories.update.status');
	Route::post('/checkCategory','Admin\CategoryController@checkCategory');


	Route::post('insertproduct', 'Admin\ProductController@insertproduct');
	Route::get('addproduct','Admin\ProductController@addproduct');
	Route::post('updateproduct', 'Admin\ProductController@updateproduct');
	Route::get('getproduct', 'Admin\ProductController@getproduct');
	Route::get('viewedit/{id}', 'Admin\ProductController@viewedit');
	Route::get('deleteproduct/{id}', 'Admin\ProductController@deleteproduct');

    Route::get('productstatus','Admin\ProductController@productstatus');
    Route::get('delteimageinupdate','Admin\ProductController@delteimageinupdate');
    
	Route::get('vieworder','Admin\OrderController@show');

	/*Route::get('deleteorder/{id}','Admin\OrderController@destroy');*/
	Route::get('/status/updateorder', 'Admin\OrderController@updateStatus')->name('orders.update.status');

	Route::get('deleteOrder/{id}','Admin\OrderController@destroy');
	//Route::get('deleteorder/{id}','Admin\OrderController@destroy');


	Route::get('logout','AdminController@logout');

	Route::get('dashboard','Admin\userController@index');
	Route::post('dashboard','Admin\userController@index');
	Route::get('delete/{id}','Admin\userController@destroy');
	Route::post('updatepassword/{id?}','Admin\userController@updatepassword');
	Route::get('editprofile/{id}','Admin\userController@editprofile');
	Route::post('updateprofile/{id}','Admin\userController@updateprofile');

	Route::any('changepassword/{id?}',function(){
		return view('admin/user/changepassword');
	});

});




