@include('admin/header')

@include('admin/nav')
<div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
  

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_settings-panel.html -->
     
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
      
         <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <ul class="navbar-nav mr-lg-2">
                    <li class="nav-item nav-search d-none d-lg-block">
                    <form method="get" action="{{url('viewproductcategory')}}" autocomplete="off">
                    <div class="input-group">
                    <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
                      <span class="input-group-text" id="search">
                        <i class="icon-search"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search By Category" aria-label="search" aria-describedby="search" name="catsearch" value="{{$sname}}">
                    </form>
                   </div>
                   </li>
                </ul><br>  
                  <center><h4 class="card-title">Category Of Laptop</h4></center>
                  <a  href="{{url('addcategory')}}" class="btn btn-info float-right" >Add Category</a>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Category Id</th>
                          <th>Category Name</th>
                          <th>Status</th>
                          <th class="action">Action</th>
                          <th  colspan="1"> </th>
                      </thead>
                      <tbody>
                        @if($display->count()>0)
                        @foreach($display as $value)
                        <tr>
                            <td>{{$value['id']}}</td>
                            <td>{{$value['category_name']}}</td>
                             <td>
                               <div class="custom-control custom-switch">

                             <input type="checkbox" data-id="{{$value->id}}" class="custom-control-input customSwitchecategory" id="customSwitches_{{$value->id}}" {{ $value->category_status == 1 ? 'checked' : '' }} >

                            
                             <label class="custom-control-label" for="customSwitches_{{$value->id}}"></label>
                           </div>
                            </td>
                            <td><a href="{{url('editcategory/'.$value['id'])}}" class="btn btn-primary">Update</a></td>
                            <td><a  href="{{url('deletecategory/'.$value['id'])}}" class="btn btn-danger delete-category" >Delete</a></td>
                        </tr>
                        @endforeach
                        @else
                         <tr>
                          <td colspan="8">
                            <center><h4 class="card-title">No Category Found</h4></center>
                          </td>
                         </tr>
                      @endif
                      </tbody>
                    </table>  
                  </div>
                </div>
              </div>

            </div>
          
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        
        <!-- partial -->
      </div>
      <div class="d-flex justify-content-center">
        {!! $display->links() !!}
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
@include('admin/footer')