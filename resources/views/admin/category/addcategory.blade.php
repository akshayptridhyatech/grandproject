@include('admin/header')
@include('admin/nav')

    <div class="container-fluid page-body-wrapper">
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
       <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper"> 
          <div class="row">
            
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add Category</h4>
    
                  <form class="forms-sample" enctype="form-data/multipart" method="post" action="{{url('insertcategory')}}" autocomplete="off" id="categoryform">
                   @csrf 

                    @if(\Session::has('success'))
                    <div class="form-row">
                        <div class="value">
                            <div class="alert alert-success">
                                <center><p>{{Session::get('success')}}</p></center>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                      <label for="exampleInputEmail3">Category Name</label>
                      <input type="text" class="form-control" id="category_name" placeholder="Enter Category Name"  name="category_name" value={{old('category_name')}}>
                      <span class="text-danger">
                        @if($errors->has('category_name'))
                                   {{ $errors->first('category_name') }}
                        @endif
                      </span>
                    </div>

                    <button type="button" onclick="checkduplicateCategory()" class="btn btn-primary mr-2">Submit</button>
                    <a  href="{{url('viewproductcategory')}}" class="btn btn-light" >Cancel</a>
                  </form>
                </div>
              </div>
            </div>
           
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
@include('admin/footer')
