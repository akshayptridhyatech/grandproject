@include('admin/header')
@include('admin/nav')
    <!-- partial:../../partials/_navbar.html -->
   
   <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper"> 
          <div class="row">
            
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Update Category</h4>
    
                  <form class="forms-sample" enctype="form-data/multipart" method="post" action="{{url('/update',$editcategory->id)}}" autocomplete="off" id="categoryform">
                   @csrf 

                    @if(\Session::has('success'))
                    <div class="form-row">
                        <div class="value">
                            <div class="alert alert-success">
                                <center><p>{{Session::get('success')}}</p></center>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                      <?php 
                      if(!empty(old('category_name'))){
                       $category_name = old('category_name');
                      }else{
                      $category_name =$editcategory->category_name;
                      }
                      ?>
                      <label for="exampleInputEmail3">Category Name</label>
                      <input type="text" class="form-control" id="category_name" placeholder="Enter Category Name" name="category_name" value="{{$category_name}}" >
                      <span class="text-danger">{{ $errors->first('category_name') }}</span>
                    </div>

                    <button type="button" onclick="duplicateCategory({{$editcategory->id}})" class="btn btn-primary mr-2">Update</button>
                    <a  href="{{url('viewproductcategory')}}" class="btn btn-light" >Cancel</a>
                  </form>
                </div>
              </div>
            </div>
           
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
@include('admin/footer')