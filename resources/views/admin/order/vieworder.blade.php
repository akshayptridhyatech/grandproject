@include('admin/header')

@include('admin/nav')

<div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_settings-panel.html -->
     
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
      
         <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <ul class="navbar-nav mr-lg-2">
                  <li class="nav-item nav-search d-none d-lg-block">
                  <form method="get" action="{{url('vieworder')}}" autocomplete="off">
                  <div class="input-group">
                  <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
                    <span class="input-group-text" id="search">
                      <i class="icon-search"></i>
                    </span>
                  </div>
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search By Product" aria-label="search" aria-describedby="search" name="ordersearch" value="{{$ordersearch}}">
                  </form>
                  </div>
                  </li>
                 </ul><br>
                  <center><h4 class="card-title">User's Order</h4></center>
                  <div class="table-responsive">
                   <table class="table">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>User Name</th>
                          <th>Product Name</th>
                          <th>Quantity</th>
                          <th>@sortablelink('amount')</th>
                           <th>Date</th>
                          <th>status</th>
                          <th>Action</th>
                         </thead>
                      <tbody>
                        @if($display->count()>0)
                        @foreach($display as $value)
                        <tr>
                            <td>{{$value['id']}}</td>
                            <td>{{$value['name']}}</td>
                            <td>{{$value['product_name']}}</td>
                            <td>{{$value['quantity']}}</td>
                            <td>{{$value['amount']}}</td>
                            <td>{{$value['created_at']}}</td>
                            <td>
                               <div class="custom-control custom-switch">
                               <input type="checkbox" data-id="{{$value->id}}" class="custom-control-input customSwitcheorder" id="customSwitches_{{$value->id}}" {{ $value->order_status == 1 ? 'checked' : '' }} >
                               <label class="custom-control-label" for="customSwitches_{{$value->id}}"></label>
                              </div>
                            </td>
                            <td><a href="{{url('deleteOrder/'.$value['id'])}}" class="btn btn-danger delete-order">Cancel Order</a></td>
                        </tr>
                        @endforeach
                        @else
                         <tr>
                          <td colspan="8">
                         <center><h4 class="card-title">No Order Found</h4></center>
                         </td>
                         </tr>
                      @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
           
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->

        <!-- partial -->
      </div>
      <div class="d-flex justify-content-center">
                  {!! $display->appends(\Request::except('page'))->render() !!}
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
@include('admin/footer')