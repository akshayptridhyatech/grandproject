@include('admin/header')
@include('admin/nav')

    
    <div class="container-fluid page-body-wrapper">
    
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
     
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                   <div class="row">
                <div class="col-md-4 mb-4 stretch-card transparent">
                  <div class="card card-tale">
                    <div class="card-body">
                      <p class="mb-4">Total User</p>
                      <p class="fs-30 mb-2">{{$user_count}}</p>
                      
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mb-4 stretch-card transparent">
                  <div class="card card-dark-blue">
                    <div class="card-body">
                      <p class="mb-4">Today's Order</p>
                      <p class="fs-30 mb-2">{{$new_order}}</p>
                    
                    </div>
                  </div>
                </div>
                <div class="col-md-4 mb-4 stretch-card transparent">
                  <div class="card card-dark-blue">
                    <div class="card-body">
                      <p class="mb-4">Total Orders</p>
                      <p class="fs-30 mb-2">{{$order_count}}</p>
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
         <div class="col-lg-12 grid-margin stretch-card">

              <div class="card">
             
                <div class="card-body">
                  <ul class="navbar-nav mr-lg-2">
                    <li class="nav-item nav-search d-none d-lg-block">
                      <form method="post" action="{{url('dashboard')}}" autocomplete="off">
                        @csrf
                      <div class="input-group">
                        <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
                          <span class="input-group-text" id="search">
                            <i class="icon-search" ></i>
                          </span>
                        </div>
                          <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search" name="search" value="{{$request->search}}">
                        </form>
                      </div>
                    </li>
                  </ul>
                  <br>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                          <center><h4 class="card-title">Users</h4></center>
                          <tr>
                            <th>Id</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th>Delete</th>
                          </tr>
                      </thead>
                          <tbody>
                            @if($users->count()>0)
                              @foreach($users as $user)
                                <tr>
                                  <td>{{$user->id}}</td>
                                  <td class="py-1"><img src="" alt="image"/></td>
                                  <td>{{$user->name}}</td>
                                  <td>{{$user->gender}}</td>
                                  <td>{{$user->email}}</td>
                                  <td>{{$user->contact}}</td>
                                  <td>{{$user->address}}</td>
                                  <td><a class="btn btn-danger delete_user" data-attr="{{$user->id}}">DELETE</a></td>
                                </tr>
                              @endforeach
                              @else
                              <tr>
                                <td colspan="8">
                              <center><h4 class="card-title">No User Found</h4></center>
                              </td>
                            </tr>
                          @endif
                          </tbody>
                          
                    </table>
                  </div>
                </div>
              </div>
            </div>
               </div> 
                </div>
              </div>
              <div class="d-flex justify-content-center">
                {!! $users->links() !!}
              </div>
            </div>


        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
      
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  
@include('admin/footer')
                                                   
                           
                            
                                                     
                          
                         
                         
                           
                      
                         
                        
                         
                           
                     
                         
                            
                          
                         
                         
                          
                          
                         
                         
                   
           