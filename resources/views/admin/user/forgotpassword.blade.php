
  @include('admin/header')

  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{asset('admin/images/lapshop_logo.png')}}" alt="logo">
              </div>
              <form class="pt-3" method="post" action="{{url('sendmail')}}" id="forgotpassword">
                @csrf
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" placeholder="Enter your email" name="email" value="{{old('email')}}">
                   <span id="error_name" class="text-danger" >
                    @if(Session::has('error'))
                      {{Session::get('error')}}
                    @endif
                    {{$errors->first('email')}}
                  </span> 
                </div>
               
                <div class="mt-3">
                  <input type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" value="Change Password">
                </div>
                <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                <a href="#" class="auth-link text-black"><center>Forgot password?</center></a>
                </div> -->
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  @include('admin/footer')


  
