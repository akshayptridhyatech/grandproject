
  @include('admin/header')

  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{asset('admin/images/lapshop_logo.png')}}" alt="logo">
              </div>
              <form class="pt-3" method="post" action="{{url('newpassword')}}" id="changepasswordForm">
                @csrf
                <div class="form-group">
                  <input type="hidden" name="id" value="{{$id}}">
                  <input type="password" class="form-control form-control-lg" id="password" placeholder="New password" name="password" value="{{old('password')}}">
                  <span id="error_name" class="text-danger" >
                      {{$errors->first('password')}}
                  </span>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="re_enter_password" placeholder="Re-enter password" name="re_enter_password" value="{{old('re_enter_password')}}">
                  <span id="error_name" class="text-danger">
                      {{$errors->first('re_enter_password')}}
                  </span>
                </div>
                <div class="mt-3">
                  <input type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" value="Change Password">
                </div>
                <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                <a href="#" class="auth-link text-black"><center>Forgot password?</center></a>
                </div> -->
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  @include('admin/footer')


  
