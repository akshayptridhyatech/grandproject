
  @include('admin/header')
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{asset('admin/images/lapshop_logo.png')}}" alt="logo">
              </div>
              <h4>Hello! let's get started</h4>
              <h6 class="font-weight-light">Sign in to continue.</h6>
              <form class="pt-3" method="POST" action="{{url('checkuser')}}" id="loginForm">
                @csrf
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username" name="email">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" name="password">
                </div>
                <div class="mt-3">
                  <!-- <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html">SIGN IN</a> -->
                  <input type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"></input>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                <a href="{{url('forgotpassword')}}" class="auth-link text-black"><center>Forgot password?</center></a>
                </div>

                    @if(\Session::has('fail'))
                    <div class="alert alert-danger">
                            <p style="color:black;">{!! \Session::get('fail')!!}</p>
                    </div>
                    @endif
                     @if ($errors->any())
                     <div class="alert alert-danger">
                    <ul>
                            @foreach ($errors->all() as $error)
                            <li style="color:black;">{{ $error }}</li>
                            @endforeach
                    </ul>
                     </div>
                    @endif
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  @include('admin/footer')


  
