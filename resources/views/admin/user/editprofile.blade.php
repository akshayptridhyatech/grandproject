
  @include('admin/header')

  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              <div class="brand-logo">
                <img src="{{asset('admin/images/lapshop_logo.png')}}" alt="logo">
              </div>
              <form class="pt-3" method="post" enctype="multipart/form-data" action="{{url('updateprofile/'.$user->id)}}" id="updateprofile">
                @csrf
                <div class="form-group">
                  <?php 
                    if(!empty(old('name'))){
                      $name = old('name');
                    }else{
                      $name = $user->name;
                    }
                  ?>
                  <input type="text" class="form-control form-control-lg" id="name" placeholder="Name" name="name" value="{{$name}}">
                  <span id="error_name" class="text-danger" >
                      {{$errors->first('name')}}
                  </span>
                </div>
                <div class="form-group">
                  <?php 
                    if(!empty(old('email'))){
                      $email = old('email');
                    }else{
                      $email = $user->email;
                    }
                  ?>
                  <input type="email" class="form-control form-control-lg" id="email" placeholder="Email" name="email" value="{{$email}}">
                  <span id="error_name" class="text-danger" >
                      {{$errors->first('email')}}
                  </span>
                </div>
                <div class="form-group">
                  <?php 
                    if(!empty(old('contact'))){
                      $contact = old('contact');
                    }else{
                      $contact = $user->contact;
                    }
                  ?>
                  <input type="number" class="form-control form-control-lg" id="contact" placeholder="Contact Number" name="contact" value="{{$contact}}" oninput="validity.valid||(value='');">
                  <span id="error_name" class="text-danger" >
                      {{$errors->first('contact')}}
                  </span>
                </div>
               <div class="form-group">
                <?php 
                    if(!empty(old('address'))){
                      $address = old('address');
                    }else{
                      $address = $user->address;
                    }
                  ?>
                  <textarea class="form-control" id="address" rows="4" name="address" placeholder="Address">{{$address}}</textarea>
                  <span id="error_name" class="text-danger" >
                      {{$errors->first('address')}}
                  </span>
                </div>
               
                <div class="form-group" >
                  <?php 
                    if(!empty(old('gender'))){
                      $gender = old('gender');
                    }else{
                      $gender = $user->gender;
                    }
                  ?>
                      <select name="gender" class="form-control">
                          <option value="">Please select</option>
                          <option value="male"  @if ("male" == $gender) {{'selected'}} @endif>Male</option>
                          <option value="female" @if ("female" == $gender) {{'selected'}} @endif>Female</option>  
                      </select>
                      <span id="error_name" class="text-danger" >
                      {{$errors->first('gender')}}
                      </span>
                </div>
                
                <div class="form-group">
                  <input type="file" name="image" class="form-control" accept="image/*">
                    <img src="{{ asset('upload/' . $user->image) }}" height="100px" width="100px" >
                    <span id="error_name" class="text-danger" >
                      {{$errors->first('image')}}
                      </span>
                </div>

                 <div class="mt-3">
                  <input type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" value="Edit profile">
                </div>
                <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                <a href="#" class="auth-link text-black"><center>Forgot password?</center></a>
                </div> -->
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  @include('admin/footer')


  
