

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
<!-- plugins:js -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> -->
  <script src="{{asset('/admin/vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <script src="{{asset('/admin/vendors/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('/admin/vendors/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('/admin/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('/admin/js/dataTables.select.min.js')}}"></script>

  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="{{asset('/admin/js/off-canvas.js')}}"></script>
  <script src="{{asset('/admin/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('/admin/js/template.js')}}"></script>
  <script src="{{asset('/admin/js/settings.js')}}"></script>
  <script src="{{asset('/admin/js/todolist.js')}}"></script>

  <!-- <script src="{{asset('/admin/js/jsvalidation.js') }}"></script> -->
  <script src="{{asset('/admin/js/category_order_validation.js') }}"></script>

  <script src="{{asset('/admin/js/productvalidation.js') }}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js
"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('/admin/js/dashboard.js')}}"></script>
  <script src="{{asset('/admin/js/Chart.roundedBarCharts.js')}}"></script>
  <script src="{{asset('/admin/js/validation.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js
"></script>
  <!-- End custom js for this page-->
   <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        
</body>

</html>