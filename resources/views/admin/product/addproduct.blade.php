@include('admin/header')
@include('admin/nav')

  
    
  @include('sweet::alert')

    <div class="container-fluid page-body-wrapper">
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper"> 
          <div class="row">
            
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Add Products</h4>

                  <!-- <p class="card-description">
                    Add product
                  </p> -->
                  <form class="forms-sample" enctype="multipart/form-data" method="post" action="{{url('insertproduct')}}" id="productform">
                     @csrf 

                    @if(Session::has('success'))
                    <div class="form-row">
                        <div class="value">
                            <div class="alert alert-success">
                                <center><p>{{Session::get('success')}}</p></center>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="form-group {{$errors->has('category') ? 'haserror':' '}}" >
                      <label for="exampleInputName1">Category</label>
                      <select name="category" class="form-control">
                           <option value="">Please select</option>
                           @if(!empty($category))
                               @foreach($category as $c)
                                 <option value="{{ $c->id }}" {{ (old("category") == $c->id? "selected":"") }}>{{ $c->category_name }}</option>
                    @endforeach
                    @endif
                           
                      </select>
                          <span class="text-danger">{{ $errors->first('category') }}</span>
                    </div>
                    <div class="form-group {{$errors->has('p_name') ? 'haserror':' '}}">
                      <label for="exampleInputEmail3">Product Name</label>
                      <input type="text" class="form-control" id="product_name" placeholder="product name" name="p_name" value="{{old('p_name')}}">
                        <span class="text-danger">{{ $errors->first('p_name') }}</span>
                    </div>

                    <div class="form-group{{$errors->has('description') ? 'haserror':' '}}">
                      <label for="exampleInputPassword4">Description</label>
                      <textarea class="form-control" id="description" rows="4"  name="description">{{old('description')}}</textarea>
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    </div>

                    <div class="form-group{{$errors->has('p_quantity') ? 'haserror':' '}}">
                      <label for="exampleInputEmail3">Quantity</label>
                      <input type="number" class="form-control" id="product_quantity" placeholder="product quantity" min=0 oninput="validity.valid||(value='');" name="p_quantity"  value="{{old('p_quantity')}}">
                        <span class="text-danger">{{ $errors->first('p_quantity') }}</span>
                    </div>
                    <div class="form-group{{$errors->has('p_price') ? 'haserror':' '}}">
                      <label for="exampleInputEmail3">Price</label>
                      <input type="number" class="form-control" id="product_price" placeholder="product price" oninput="validity.valid||(value='');" name="p_price" value="{{old('p_price')}}">
                        <span class="text-danger">{{ $errors->first('p_price') }}</span>
                    </div>
                     <div class="form-group{{$errors->has('file') ? 'haserror':' '}}">
                      <label>Product image</label>
                      <input type="file"  name="file[]"  class="form-control" value="{{old('file')}}" multiple/>
                               
                        <span class="text-danger">{{ $errors->first('file') }}</span>
                    </div>

                   
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-light"> <a href="{{url('getproduct/')}}">Cancel</button>
                  </form>
                </div>
              </div>
            </div>
           
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->

        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>

 
@include('admin/footer')