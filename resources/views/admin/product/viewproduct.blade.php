@include('admin/header')
@include('admin/nav')

    <!-- partial -->
    @include('sweet::alert')
    <div class="container-fluid page-body-wrapper">
      <!-- partial:../../partials/_settings-panel.html -->
     
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')

      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper">
         
          <div class="row">
           
      
         <div class="col-12 grid-margin stretch-card">

              <div class="card">

                <div class="card-body">
            <ul class="navbar-nav mr-lg-2">
              <li class="nav-item nav-search d-none d-lg-block">
                <form method="get" action="{{url('getproduct')}}">
                <div class="input-group">
                  <div class="input-group-prepend hover-cursor" id="navbar-search-icon">
                    <span class="input-group-text" id="search">
                      <i class="icon-search" ></i>
                    </span>
                  </div>
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search for product" name="sproduct"  value="{{$filter}}"aria-label="search" aria-describedby="search">
                  </form>
                </div>
              </li>
            </ul><br><br>
                  <h4 class="card-title"><center>Product</center></h4>
                  <a class="btn btn-info float-right" href="{{url('addproduct/')}}">Add Product</a>
                  <br><br>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>product images</th>
                          <th>Category</th>
                          <th>Name</th>
                          <th>Description</th>
                           <th>Quantity</th>
                           <th>Price</th>
                           <th>Status</th>
                          <th class="action">Action</th>
                          

                      </thead>
                 
                      <tbody>

                        @if($product1->count()>0)
                         @foreach($product1 as $p)
                     
                        <tr>
                        

                          <td>
                       
                          @foreach($p->setimages as $value)
                            <img src="{{asset('upload/'.$value->image_name) }}" style="height:120px;width:120px;" alt="image"/>
                         
                            @endforeach
                          </td>
                          <td>{{$p['category_name']}}</td>
                          <td>{{$p['product_name']}}</td>
                          <td>{{$p['product_description']}}</td>
                          <td>{{$p['product_quantity']}}</td>
                          <td>{{$p['product_price']}}</td>
               

                           <td>
                            <div class="custom-control custom-switch">
                          <input type="checkbox" data-id="{{$p->id}}" class="custom-control-input customSwitchesproduct" id="customSwitches_{{$p->id}}" {{ $p->product_status == 1 ? 'checked' : '' }} >
                          <label class="custom-control-label" for="customSwitches_{{$p->id}}"></label>
                          </div>
                           </td>
                          <td><a class="btn btn-primary" href="{{url('/viewedit/'.$p['id'])}}">Update</a>
                          <a class="btn btn-danger delete-product" href="{{url('deleteproduct/'.$p['id'])}}">Delete</a></td>
                        </tr>
                        @endforeach
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
           
       
      </div>
      <div class="d-flex justify-content-center">
            {!! $product1->links() !!}
           </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <style type="text/css">
    
  </style>
  <script>


  

</script>

@include('admin/footer')

