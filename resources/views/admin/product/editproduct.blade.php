
@include('admin/header')

@include('admin/nav')


    <!-- partial -->
    @include('sweet::alert')
    <div class="container-fluid page-body-wrapper">
      
      <div id="right-sidebar" class="settings-panel">
        <i class="settings-close ti-close"></i>
      </div>
      <!-- partial -->
      <!-- partial:../../partials/_sidebar.html -->
      @include('admin/sidebar')
      <!-- partial -->
      <div class="main-panel">        
        <div class="content-wrapper"> 
          <div class="row">
            
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Products</h4>
                  <!-- <p class="card-description">
                    Add product
                  </p> -->
                  <form class="forms-sample" enctype="multipart/form-data" method="post" action="{{url('updateproduct')}}" id="productform">
                     @csrf 

                    @if(\Session::has('success'))
                    <div class="form-row">
                        <div class="value">
                            <div class="alert alert-success">
                                <center><p>{{Session::get('success')}}</p></center>
                            </div>
                        </div>
                    </div>
                    @endif
                    <input type="hidden" name="product_id" value="{{$data->id}}">
                    <div class="form-group {{$errors->has('category') ? 'haserror':' '}}" >
                      <label for="exampleInputName1">Category</label>
                      <select name="category" class="form-control">
                           <option value="">Please select</option>
                           @if(!empty($category))
                               @foreach($category as $c)
                                 <option value="{{ $c->id }}"@if($c->id==$data->category_id) {{ 'selected'  }}@endif>{{ $c->category_name }}</option>
                    @endforeach
                    @endif
                           
                      </select>
                          <span class="text-danger">{{ $errors->first('category') }}</span>
                    </div>
                    <div class="form-group {{$errors->has('p_name') ? 'haserror':' '}}">
                      
                      <label for="exampleInputEmail3">Product Name</label>
                      <input type="text" class="form-control" id="product_name" placeholder="product name" name="p_name" value="{{$data->product_name}}">
                        <span class="text-danger">{{ $errors->first('p_name') }}</span>
                    </div>
                    <div class="form-group{{$errors->has('description') ? 'haserror':' '}}">
                      <label for="exampleInputPassword4">Description</label>
                      <textarea class="form-control" id="description" rows="4" name="description">{{$data->product_description}}</textarea>
                        <span class="text-danger">{{ $errors->first('description') }}</span>
                    </div>
                    <div class="form-group{{$errors->has('p_quantity') ? 'haserror':' '}}">
                      <label for="exampleInputEmail3">Quantity</label>
                      <input type="number" class="form-control" id="product_quantity" placeholder="product quantity" min=0 oninput="validity.valid||(value='');" name="p_quantity" value="{{$data->product_quantity}}">
                        <span class="text-danger">{{ $errors->first('p_quantity') }}</span>
                    </div>
                    <div class="form-group{{$errors->has('p_price') ? 'haserror':' '}}">
                      <label for="exampleInputEmail3">Price</label>
                      <input type="number" class="form-control" id="product_price" placeholder="product price" oninput="validity.valid||(value='');" name="p_price" value="{{$data->product_price}}">
                        <span class="text-danger">{{ $errors->first('p_price') }}</span>
                    </div>
                    <div class="form-group{{$errors->has('file') ? 'haserror':' '}}">
                      <label>Product image</label>
                      <input type="file"  name="file[]"  class="form-control" multiple="">
                         
                        @foreach ($images as $image)
                        <div class="hide_{{$image->id}}">
                       
                      <br><img src="{{asset('upload/'.$image->image_name) }}" data-attr="{{$image->id}}" class="remove"style="height:120px;width:120px;" alt="image" />&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button  class="btn btn-danger deleteimage" data-attr="{{$image->id}}">Delete</button><br>
                    </div>
                      @endforeach
                        <span class="text-danger">{{ $errors->first('p_image') }}</span>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Update</button>
                   
                  </form>
                </div>
              </div>
            </div>
           
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
     
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <script>
  

  </script>
  
    
   
        
@include('admin/footer')
