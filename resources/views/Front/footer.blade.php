
<script type="text/javascript" src="{{asset('Front/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('Front/js/easing.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js
"></script>
<script src="{{asset('Front/js/skdslider.min.js')}}"></script>
<link href="{{asset('Front/css/skdslider.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{asset('Front/js/uservalidation.js')}}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
		 $('.nav li').hover(
        function () {
            //show submenu
            $('ul', this).show();
        }, function () {
            //hide submenu
            $('ul', this).hide();
        });
	});


		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
	
</script>
</body>
</html>