@include('Front/header')

 @include('Front/nav')
 	<ul id="demo1">
		<li>
			<img src="{{asset('Front/images/laptops/img13.jpg')}}" alt="" />
		</li>
		<li>
			<img src="{{asset('Front/images/laptops/img12.jpg')}}" alt="" />
			
		</li>
		
		<li>
			<img src="{{asset('Front/images/laptops/img1.jpg')}}" alt="" />
			
		</li>
		
</ul> 


		<div class="top-brands">
			<div class="container">
					<h2>Latest Products</h2>
						<div class="grid_3 grid_5">
							<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
							  <div id="myTabContent" class="tab-content">
									<div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
						               @foreach($product as $products)
					
										<div class="agile_top_brands_grids">
											<div class="col-md-4 top_brand_left">
												<div class="hover14 column">
													
													<div class="agile_top_brand_left_grid">
													 
														<div class="agile_top_brand_left_grid1">
															<figure>
																<div class="snipcart-item block" >
																  <div class="snipcart-thumb">
																	<a href="products.html"><img title=" " alt=" " src="{{asset('upload/'.
																		$products->first_pic->image_name)}}" height="150" width="150"/></a>
																	
																	<p>{{$products->product_name}}</p>
																	
																	<h4><i class="fa fa-inr"></i>{{$products->product_price}}</h4>
																	<p>{{$products->product_description}}</p>
																   </div>
						

                                                        
															
														    <div class="snipcart-details top_brand_home_details">
															<form action="#" method="post">
																<fieldset>
																	<input type="hidden" name="cmd" value="_cart" />
																	<input type="hidden" name="add" value="1" />
																	<input type="hidden" name="business" value=" " />
																	<input type="hidden" name="item_name" value="Fortune Sunflower Oil" />
																	<input type="hidden" name="amount" value="20.99" />
																	<input type="hidden" name="discount_amount" value="1.00" />
																	<input type="hidden" name="currency_code" value="USD" />
																	<input type="hidden" name="return" value=" " />
																	<input type="hidden" name="cancel_return" value=" " />
																	<input type="submit" name="submit" value="Add to cart" class="button" />
																</fieldset>
															</form>
														</div>
													</div>
												</figure>
											</div>
											
										</div>
									</div>
								</div>
								@endforeach 
							<div class="clearfix"> </div>
						</div>
					</div>
								
						<div class="clearfix"> </div>
						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
								
				
				
				
					
	<script src="{{asset('Front/js/skdslider.min.js')}}"></script>
<link href="{{asset('Front/css/skdslider.css')}}" rel="stylesheet">
<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			
		});
</script>	
@include('Front/footer')
@include('Front/footer_div')
