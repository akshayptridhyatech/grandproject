@include('Front/header')
	<div class="login">
		<div class="container">
			<h2>Reset Password Form</h2>
		
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form method="post" action="{{url('newpasswordofuser')}}" id="resetpasswordform">
					@csrf
                  	<input type="hidden" name="id" value="{{$id}}">
					<input type="password" placeholder="New password" name="password">
					<span class="text-danger">{{$errors->first('password')}}</span>
					<input type="password" placeholder="Re enter password" name="confirm_password">
					<span class="text-danger">{{$errors->first('confirm_password')}}</span>
					@if(Session::has('error'))
						<span class="text-danger">{{Session::get('error')}}</span>
					@endif
					<input type="submit" value="Reset Password">
				</form>
			</div>
		</div>
	</div>
@include('Front/footer')
