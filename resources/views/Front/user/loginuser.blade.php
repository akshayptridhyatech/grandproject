@include('Front/header')

<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Login Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- login -->
	<div class="login">
		<div class="container">
			<h2>Login Form</h2>
		
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form method="post" action="{{url('/authenticateuser')}}" id="loginform">
					@csrf
					@if(Session::has('fail'))
					<div class="alert alert-danger" role="alert">
						<span class="text-danger">{{Session::get('fail')}}</span>
					</div>
					@endif
					<input type="email" placeholder="Email Address" name="email">
					<span class="text-danger">{{$errors->first('email')}}</span>

					<input type="password" placeholder="Password" name="password">
					<span class="text-danger">{{$errors->first('password')}}</span>

					<div class="forgot">
						<a href="{{url('getmail')}}">Forgot Password?</a>
					</div>
					<input type="submit" value="Login">
				</form>
			</div>
			<h4>For New People</h4>
			<p><a href="{{url('register')}}">Register Here</a> (Or) go back to <a href="{{url('index')}}">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
		</div>
	</div>
<!-- //login -->
@include('Front/footer')