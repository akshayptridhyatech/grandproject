@include('Front/header')
<div class="register">
		<div class="container">
			<h2>Register Here</h2>
			<div class="login-form-grids">
				<h5>profile information</h5>
				<form action="{{url('/adduser')}}" method="post" enctype="multipart/form-data" id="register">
					@csrf
					<input type="text" placeholder="Name" name="name" value="{{old('name')}}">
					<span class="text-danger"> {{$errors->first('name')}}</span>
					<br>

					<input type="email" placeholder="Email Address" name="email" value="{{old('email')}}">
					<span class="text-danger">{{$errors->first('email')}}</span>
					<br>

					<input type="number" name="contact" oninput="validity.valid||(value='');" placeholder="Contact Number" value="{{old('contact')}}">
					<span class="text-danger"> {{$errors->first('contact')}}</span>
					</br>

					<select name="gender">
						<option>Please select</option>
						<option value="male" @if(old('gender')=='male'){{"selected"}}@endif>Male</option>
						<option value="female" @if(old('gender')=='female'){{"selected"}}@endif>Female</option>
					</select>
					<span class="text-danger"> {{$errors->first('gender')}}</span>
					<br>

					<textarea rows="4" class="form-data" name="address" placeholder="Address">{{old('address')}}</textarea>
					<span class="text-danger"> {{$errors->first('address')}}</span>
					<br>

					<input type="password" placeholder="Password" name="password" >
					<span class="text-danger"> {{$errors->first('password')}}</span>
					<br>

					<input type="password" placeholder="Password Confirmation" name="confirm_password">
					<span class="text-danger"> {{$errors->first('confirm_password')}}</span>
					<br>

					<input type="file" name="image" >
					<span class="text-danger"> {{$errors->first('image')}}</span>

					<input type="submit" value="Register">
					<br>
					<p><a href="{{url('signin')}}">Already have an account?</a> (Or) go back to <a href="{{url('index')}}">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
				</form>
			</div>
			<div class="register-home">
				<a href="index.html">Home</a>
			</div>
		</div>
	</div>
@include('Front/footer')