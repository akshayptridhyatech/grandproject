@include('Front/header')
	<div class="login">
		<div class="container">
			<h2>Forgt Password Form</h2>
		
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form method="post" action="{{url('sendmailtouser')}}">
					@csrf
					<input type="email" placeholder="Email Address" name="email">
					<span class="text-danger">{{$errors->first('email')}}</span>
					@if(Session::has('error'))
						<span class="text-danger">{{Session::get('error')}}</span>
					@endif
					<input type="submit" value="Get Mail">
				</form>
			</div>
		</div>
	</div>
@include('Front/footer')
