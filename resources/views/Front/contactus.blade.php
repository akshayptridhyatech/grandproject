@include('Front/header')
 @include('Front/nav') 
 <!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Contact</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- contact -->
	<div class="about">
		<div class="w3_agileits_contact_grids">
			<div class="col-md-6 w3_agileits_contact_grid_left">
				<div class="agile_map">
					<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.3905851087434!2d-34.90500565012194!3d-8.061582082752993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab18d90992e4ab%3A0x8e83c4afabe39a3a!2sSport+Club+Do+Recife!5e0!3m2!1sen!2sin!4v1478684415917" style="border:0"></iframe> -->
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14682.872284944237!2d72.5177052!3d23.0707954!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbc02189d8f4c72a3!2sTridhya%20Tech%20Private%20Limited!5e0!3m2!1sen!2sin!4v1616671863225!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
				<div class="agileits_w3layouts_map_pos">
					<div class="agileits_w3layouts_map_pos1">
						<h3>Contact Info</h3>
						<p>1113-1121,iSquare, Before Shukan Mall, Science City Rd.</p>
						<ul class="wthree_contact_info_address">
							<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">info@tridhya.com</a></li>
							<li><i class="fa fa-phone" aria-hidden="true"></i>998 232 232</li>
						</ul>
						<div class="w3_agile_social_icons w3_agile_social_icons_contact">
							<ul>
								<li><a href="https://www.facebook.com/TridhyaTech" class="icon icon-cube agile_facebook"></a></li>
								<li><a href="#" class="icon icon-cube agile_rss"></a></li>
								<li><a href="#" class="icon icon-cube agile_t"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 w3_agileits_contact_grid_right">
				<h2 class="w3_agile_header">Leave a<span> Message</span></h2>

				<form action="{{url('/sendmailtolapshop')}}" method="post">
					@csrf
					<span class="input input--ichiro">
						<input class="input__field input__field--ichiro" type="text"  name="name" placeholder=" "  id="input-25"/>
						<label class="input__label input__label--ichiro" for="input-25">
							<span class="input__label-content input__label-content--ichiro">Your Name</span>
						</label>
					</span>
					<span class="text-danger">{{$errors->first('name')}}</span>
					<span class="input input--ichiro">
						<input class="input__field input__field--ichiro" type="email"  name="email" placeholder=" " id="input-26" />
						<label class="input__label input__label--ichiro" for="input-26">
							<span class="input__label-content input__label-content--ichiro">Your Email</span>
						</label>
					</span>
					<span class="text-danger">{{$errors->first('email')}}</span>
					<textarea name="message" placeholder="Your message here..."></textarea>
					<span class="text-danger">{{$errors->first('message')}}</span>
					<input type="submit" value="Submit">
				</form>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- contact -->
@include('Front/footer')