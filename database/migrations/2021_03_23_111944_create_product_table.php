<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id('id');
            $table->integer('category_id');
            $table->string('product_name');
            $table->text('product_description');
            $table->integer('product_quantity');
            $table->integer('product_price');
            $table->string('product_image');
          $table->string('product_status')->comment = '0-product not avaliable 1-product available';

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
